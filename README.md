# Project description #

## Problem condition: ##
it is necessary to construct a neural network that solves the inverse problem - that is, to find the stiffness coefficient k at the deviations x

## How to run project: ##
1. Sign up to Colab [link to Colab](https://colab.research.google.com)
2. Copy there source code **network.ipynb** and **Data.xlsx** as a sample data
3. Run project and analyze results
